import csv
import json
import xlsxwriter
import os
import csv
import re

import xlrd


current_row = 0
sheet_num = 0
input_total = 0
output_total = 0

# path to the file you want to extract data from
src = 'garbage.xlsx'

book = xlrd.open_workbook(src)

# select the sheet that the data resids in
work_sheet = book.sheet_by_index(sheet_num)

# get the total number of rows
# num_rows = work_sheet.nrows - 1

workbook = xlsxwriter.Workbook('garbage_new.xlsx')
worksheet_out = workbook.add_worksheet('Batch6') 
cur_row=0

num_rows = work_sheet.nrows

while current_row < num_rows:

	col_0=work_sheet.cell_value(current_row, 0)
	col_1= work_sheet.cell_value(current_row, 1)
	col_2= work_sheet.cell_value(current_row, 2)
	col_3= work_sheet.cell_value(current_row, 3)
	col_4= work_sheet.cell_value(current_row, 4)
	col_5= work_sheet.cell_value(current_row, 5)
	col_6=work_sheet.cell_value(current_row, 6) 
	col_7=work_sheet.cell_value(current_row, 7) 

	
	worksheet_out.write(cur_row,0,col_0)
	worksheet_out.write(cur_row,1,col_1)
	worksheet_out.write(cur_row,2,col_2)
	worksheet_out.write(cur_row,3,col_3)
	worksheet_out.write(cur_row,4,col_4)
	


	norm_unit_list = ['mg','mgs','ug','%','nm','bda','iu','mcg','(mcg)','(mg)','g','(g)','gms','gm','(gms)','gram','grams','kj','kcal','%','kJ','ml','packet', 'stick', 'Bag', 'Can', 'Cup', 'Bar', 'Package', 'Bottle', 'Pouch', 'oz(Ounce)', 'Pieces', 'tbsp', 'Box']
	nil_val=['trace','traces','nil','bdl']
	print current_row
	if '%' in str(col_6):
		col_6=''
	if '%'  in str(col_7):
		col_7=''
	mod_col_5=re.sub('[^A-Za-z.% ]+', '', col_5)
	col_6=str(col_6).lower()
	col_6=col_6.replace(" ","")
	special_energy_flag=0

	if any(null_val in col_6 for null_val in nil_val):
		col_6='0'
	if '/' in col_6:
		col_6=re.sub('\/.*','',col_6)
	col_6=re.sub('[^A-Za-z.0-9% ]+', '', col_6)
	col_7=str(col_7).lower()
	col_7=col_7.replace(" ","")
	if any(null_val in col_7 for null_val in nil_val):
		col_7='0'
	if '/' in col_7:
		col_7=re.sub('\/.*','',col_7)
	col_7=re.sub('[^A-Za-z.0-9% ]+', '', col_7)
	print mod_col_5
	flag=0
	unit_val=""
	if col_6=='' and col_7=='':
		current_row+=1
		continue
	for u in norm_unit_list:
		if " "+u in mod_col_5.lower():
			unit_val=u
			flag=1
			break
	if flag==1:
		x1=""
		x2=""
		for u in norm_unit_list:
			if x1!="" or x2!="":
				break
			if u in col_6.lower() and col_6!="": 
				x1=u
			if u in col_7.lower() and col_7!="":
				x2=u
		if x1!="" and unit_val!=x1:
			mod_col_5=mod_col_5.replace(u,"")
			mod_col_5+=" "+x1
			col_6=col_6.replace(x1,"")
			flag=2
		if x2!="" and unit_val!=x2:
			mod_col_5=mod_col_5.replace(u,"")
			mod_col_5+=" "+x2
			col_7=col_7.replace(x2,"")
			flag=2


	if flag==0:
		for u in norm_unit_list:
			if flag==1:
				break
			if u in col_6.lower() and col_6!="": 
				mod_col_5=mod_col_5+" "+u
				col_6=col_6.replace(u,"")
				flag=1
			if u in col_7.lower() and col_7!="":
				if flag!=1:
					mod_col_5=mod_col_5+" "+u
					flag=1
				col_7=col_7.replace(u,"")

	col_6=re.sub('[A-Za-z]', '', col_6)
	col_7=re.sub('[A-Za-z]', '', col_7)

	worksheet_out.write(cur_row,5,mod_col_5)
	worksheet_out.write(cur_row,6,col_6)
	worksheet_out.write(cur_row,7,col_7)
	
	current_row+=1
	cur_row=cur_row+1
workbook.close()