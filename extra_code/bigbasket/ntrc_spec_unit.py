# -*- coding: utf-8 -*-
import csv
import json
import xlsxwriter
import os
import csv
import xlrd
import re
from operator import itemgetter

src="./bigbasket_nutri_product_all.json"



# Nutrition Table Classifier based on nutrition words
# It counts number of nutrition key words present.
# If the number exceed thresh value, it is declared as nutritin table.
# thresh=3 gave best result for bigbasket
def checkNT(specs,thresh=3):
	headWordList = [ 'Nutrition','Facts','Protien','Fat',
			'Sodium','Iron','Size','Serving',
			'Container','Amount','Calorie','Saturated','Trans',
			'Cholesterol','Sodium','Carbohydrate','Dietary','Fiber',
			'Sugar','Protein','Calcium','Iron','Vitamin',
			'Thiamin','Riboflavin','Niacin','VitaminB',
			'Folic','Acid','Zinc','Percent','Values','Polyunsaturated',
			'Monounsaturated','Potassium','Folate','Biotin','Panthothenate',
			'Phosphorus','Iodine','Magnesium',
			'Selenum','Copper','Manganese',
			'Chromium','Molybdenum','Chloride','alcohol','Soluble',
			'Insoluble','Salt','Energy'
			]
	count=0
	for j in headWordList:
		if j.lower() in specs.lower():
			count+=1
	if count>=thresh:
		return True
	return False


# Remove noise after the nutrition table
def getNTinfoEnd(spec):
	end_List=['EAN Code','2000 kcal per day','Directions','Add 1/2 cup of Oats']
	# Remove Extra from end of nutrition info
	chars = "`*_[]()>#+-!$"
	for c in chars:
		spec_trunc= spec.lower().replace(c, " ")
	for priority_word in end_List:
		try:
			end_index=spec_trunc.index(priority_word.lower())
			break
		except:
			end_index=len(spec)
	if checkNT(spec[end_index:],thresh=2)==True:
		end_index=len(spec)
	spec=spec[:end_index]
	return spec

# Remove noise from before nutrition table
def getNTinfoStart(spec):
	start_List=[' per ','approximate','serving','erving','energy','calorie','nutrition']
	# Remove Extra before nutrition info
	chars = "`*_[]()>#+-!$.,:<>"
	spec_trunc=spec
	for c in chars:
		spec_trunc= spec_trunc.replace(c, " ")
	# Remove extra from beginning according to priority in start_List
	for priority_word in start_List:
		starts = [match.start() for match in re.finditer(re.escape(priority_word.lower()), spec_trunc.lower())]
		if len(starts)!=0:
			if priority_word=='calorie':
				spec=spec[starts[0]:]
			else:
				spec=spec[starts[-1]:]
			return spec
	return spec

# Returns count of unit available in the spec: specification
def unitDetector(spec):
	unitsList=['g','gram','gm','gms','grams','mg','milligram','ml','kcal','kj','l']
	chars = "`*_[]()>#+-!$"
	for c in chars:
		spec= spec.replace(c, " ")
	# print spec
	count=0
	for unit in unitsList:
		listfound=re.findall(' '+unit+' ', spec.lower())
		listfound2=re.findall(r'\d+\.*\d*'+unit, spec.lower())
		count+=len(listfound)
		count+=len(listfound2)
	print "Count:"+str(count)
	return count


# Nutrition table classifier
# First check if it has nutrition words
# If true, check if it has more than 2 units of measurement.
# If both cnditions are true, it is nutrition table
def nutriClassifier(outFile='ntrc.txt'):
	prod_count=0
	with open(src,"r") as f:
		with open(outFile,'w') as fout:
			for n,i in enumerate(f):
				data=json.loads(i)
				try:
					specs=data['specifications']
					specs=''.join([i if ord(i) < 128 else '' for i in specs])
					if checkNT(specs)==True:
						c=unitDetector(specs)
						print "c:"+str(c)
						if c>2:
							print "NTR found"
							print specs
							fout.write(data['urlh']+"###"+specs)
							fout.write("\n")
							prod_count=prod_count+1
						else:
							print "NTR not found"
				except Exception as e:
					print e
					print ""
	print "Product Count"
	print prod_count


# Truncate description before and after nutrition info to remove excess information
def nutritrunc(srcFile='ntrc.txt',outFile='ntTrunc.txt'):
	with open(srcFile,"r") as f:
		count=0
		with open(outFile,"w") as fout:
			for line in f:
				print ""
				# print spec
				urlh=line.split("###")[0]
				spec=line.split("###")[1]
				spec_trunc=getNTinfoEnd(spec)
				spec_trunc=getNTinfoStart(spec_trunc)
				# print "Truncated"
				print spec_trunc
				count+=1
				fout.write(urlh+"###"+spec_trunc.strip("\n"))
				fout.write("\n")
		return count


# Segment truncated description to get key-value pairs in excel sheet.
def segment(src='ntTrunc.txt',dest='output.xlsx'):
	workbook = xlsxwriter.Workbook(dest)
	worksheet_out = workbook.add_worksheet('Bigbasket') 
	write_cur_row=0
	gh=0
	with open(src,"r") as fin:
		for line in fin:
			spec=line.split("###")[1]
			chars = "`*_[]()>#+-!$:,<>@?|/"
			for c in chars:
				spec= spec.replace(c, " ")
			unitsList=['kcal','kj','l','iu','bags','%','gram','gm','gms','grams','mg','mcg','g','milligram','ml']
			
			# Replace trace and nil with 0 values
			spec=spec.replace("Traces","0")
			spec=spec.replace("Nil","0")
			spec=spec.replace("traces","0")
			spec=spec.replace("NIL","0")
			# Remove extra spaces
			spec=re.sub( '\s+', ' ', spec ).strip()
			print spec

			# Split line on the basis of digits and digits+units
			numbers=[(m.start(0), m.end(0)) for m in re.finditer('\d+[.\d]*', spec)]
			numbers_dict=[]
			for iterator in numbers:
				numbers_dict.append({spec[iterator[0]:iterator[1]]:iterator})
			final_unit_dict=[]
			for unit in unitsList:
				l=[]
				l.extend([(m.start(0)+1, m.end(0)-1) for m in re.finditer(" "+unit.lower()+" ", spec.lower()+" ")])
				l.extend([(m.start(0)+1, m.end(0)-1) for m in re.finditer(r'\d'+unit.lower()+" ", spec.lower()+" ")])
				for iterator in l:
					final_unit_dict.append({unit:iterator})

			# Combine numbers with units
			# Combination in form of [number,unit, start index, end index]
			combination=[]
			for eachnum in numbers_dict:
				number=float(eachnum.keys()[0].strip('.'))
				number_beg=eachnum[eachnum.keys()[0]][0]
				number_end=eachnum[eachnum.keys()[0]][1]
				flag=0
				for eachunit in final_unit_dict:
					if flag==1:
						break
					# unit after number
					if eachunit[eachunit.keys()[0]][0]-number_end<=2 and eachunit[eachunit.keys()[0]][0]>=number_end:
						combination.append([str(number),eachunit.keys()[0],number_beg,eachunit[eachunit.keys()[0]][1]])
						flag=1
					# unit before number
					elif number_beg-eachunit[eachunit.keys()[0]][1]<=2 and eachunit[eachunit.keys()[0]][1]<=number_beg:
						combination.append([str(number),eachunit.keys()[0],eachunit[eachunit.keys()[0]][0],number_end])
						flag=1
				if flag==0:
					combination.append([str(number),'',number_beg,number_end])

			# Segment text on the basis of combinations found
			### sort combination on the basis of starting index
			nutrition_info=[]
			combination=sorted(combination, key=itemgetter(2))
			currentIndex=0
			per_100=False
			for eachcomb in combination:
				if eachcomb[0]=='100.0':
					if 'gram' in eachcomb[1] or 'gm' in eachcomb[1] or 'g' in eachcomb[1]:
						per_100=True
						break
			for eachcomb in combination:
				text=spec[currentIndex:eachcomb[2]].strip()
				if eachcomb[1]!="":
					text=text+" ("+eachcomb[1]+") "
				number=eachcomb[0]
				currentIndex=eachcomb[3]
				print text+":"+number
				if per_100==True:
					nutrition_info.append([text,'',number])
				else:
					nutrition_info.append([text,number,''])
			print nutrition_info

			# Write to excel sheet
			urlh=line.split("###")[0]
			worksheet_out.write(write_cur_row,0,urlh)
			title=''
			meta=''
			with open("bigbasket_nutri_product_all.json","r") as fread:
				for eachProduct in fread:
					d=json.loads(eachProduct) 
					if d['urlh']!=urlh:
						continue
					title=d['title']
					meta=d['meta']
					specification_orig=d['specifications']
					break
			worksheet_out.write(write_cur_row,1,meta)
			worksheet_out.write(write_cur_row,2,title)
			worksheet_out.write(write_cur_row,10,specification_orig)
			write_cur_row+=1

			for nutri_line in nutrition_info:
				worksheet_out.write(write_cur_row,6,nutri_line[0])
				worksheet_out.write(write_cur_row,7,nutri_line[1])
				worksheet_out.write(write_cur_row,8,nutri_line[2])
				write_cur_row+=1

			write_cur_row+=3
			worksheet_out.write(write_cur_row,0,"############")
			worksheet_out.write(write_cur_row,3,"############")
			write_cur_row+=2


if __name__=='__main__':
	# Classifies if the description contains nutrition info or not. 
	# If present, write urlh and specs to ntrc.txt
	nutriClassifier()

	# Specification are truncated from beginning and end to remove extra noise and get nutrition info only
	# Input-ntrc.txt, Output-ntTrunc.txt
	nutritrunc()

	# Extract key value pairs from truncated specification and put into excel sheet for QA verification
	segment()
