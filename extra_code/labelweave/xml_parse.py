from lxml import etree
from StringIO import StringIO
import cv2
import os
import math
 

def parseBookXML(xmlFile):
 
	f = open(xmlFile)
	xml = f.read()
	f.close()
 
	tree = etree.parse(StringIO(xml))
	print tree.docinfo.doctype
	context = etree.iterparse(StringIO(xml))
	anot_dict = {}
	books = []
	i=0
	for action, elem in context:
		if not elem.text:
			text = "None"
		else:
			text = elem.text
		print elem.tag + " => " + text
		if elem.tag=='x':
			i+=1
			anot_dict["point_"+str(i)]={}

		if elem.tag=='x' or elem.tag=='y':
			anot_dict["point_"+str(i)][elem.tag] = math.ceil(float(text))
		else:
			anot_dict[elem.tag]=text
	return anot_dict

def cropimg(src,dest,anot_dict):
	img=cv2.imread(src,1)
	print anot_dict
	crop_img=img[int(anot_dict['point_1']['y']):int(anot_dict['point_3']['y']),int(anot_dict['point_1']['x']):int(anot_dict['point_3']['x'])]
	# if not os.path.exists("/".join(dest.split('/')[:-1])):
	# 	os.makedirs("/".join(dest.split('/')[:-1]))
	cv2.imwrite(dest+".jpg",crop_img)

 
if __name__ == "__main__":
	#src- image dirctory, xml-src-annotations frm labelweave
	src="./labelweave_crop/NTR28/"
	xml_src="./annot/NTR28/"
	for root, subFolder, files in os.walk(xml_src):
		for item in files:
			try:
				print item
				
				anot_dict=parseBookXML(xml_src+item)
				dest="./cropped_23_2/"+item.split('.')[0]+"_"+anot_dict['name']
				cropimg(src+item.split('.')[0]+".jpg",dest,anot_dict)
			except:
				continue

