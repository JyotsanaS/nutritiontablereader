import csv
import os
import json
import re
import xlsxwriter

src_folder="./21May/P45/"
# src_folder="./exp/"
# dest_folder="./final_csv_output/"


workbook = xlsxwriter.Workbook('Batch_P45.xlsx')
# workbook = xlsxwriter.Workbook('exp.xlsx')
worksheet_out = workbook.add_worksheet('QAVerification') 
cur_row=0


# count=0
count_file=0
for file in os.listdir(src_folder):
	# if i>20:
	# 	break
	# print file
	# i+=1
	# print src_folder+file
	# try:
	count_file+=1
	# if count_file<400:
	# 	continue
	# if count_file>500:
	# 	break
	
	with open(src_folder+file,'r') as fin:
		
		print file
		reader = csv.reader(fin)
		flag=0
		csv_data=list(reader)
		# print csv_data
		norm_serv_size=["size","serving"]
		not_norm_serv_size=["container"]
		norm_serv_cont=["per container","per package","contains"]
		amt_per_serving=["amount per serving","amt"]
		norm_ntr=["calories","energy","saturated fat","vitamin","total fat","fat","trans fat","fat","trans","poly unsaturated fat","unsaturated fat","mono unsaturated fat","unsat fat","dietary fiber","sugar","fiber","solublefiber","other carbohydrate","total sugar","insolublefiber","calc",
		"calories","omega","calories from fat","protein","fat","total fat","carbohydrate","sugar","sugars,of which","energy","vitamin","saturated fat","carbohydrate","trans fat","cholesterol","sodium","total carbohydrate","dietary fiber","energy from fat","total fat","saturated fatty acids","monounsaturated fatty acids","polyunsaturated fatty acids","vitamin a","vitamin c","vitamin b1","vitamin b2","vitamin b3","vitamin b6","vitamin b12","folate","iron","soluble fiber","insoluble", "fiber","thiamine","riboflavin","niacin","zinc","copper","magnesium","folic acid","phosphorous","calcium","iodine","inositol","choline","potassium","chromium","molybdenum","taurine","polyunsaturated fat","monounsaturated fat","vitamin d","fat","trans","cholesterol","sodium","carbohydrate","dietary" ,"fiber","energy","total fat","saturated","mono", "unsaturated","polyunsaturated"]
		norm_unit_list = ['mgs','mg','mcg','gms','(g)','gram','g','ug','grams','kj','kcal','%','kJ','ml','packet', 'stick', 'Bag', 'Can', 'Cup', 'Bar', 'Package', 'Bottle', 'Pouch', 'oz(Ounce)', 'Pieces', 'tbsp', 'Box']
		ntr_morethan1=['saturated fat','total fat','trans fat','poly unsaturated fat','polyunsaturated fat','total cal']
		norm_header=["amount per serving","daily value","dv","100g","100 g"]
		amt_number=[]
		each_100_number=[]
		serv_size=[]
		serv_container=[]
		header=[]
		# Nutr_table
		start_flag=0
		header=0
		nut_info=[]
		print "Before"
		for rownum,row in enumerate(csv_data):
			print row

		for rownum,row in enumerate(csv_data):
			if any(value.lower() in row[0].lower() for value in norm_ntr) and len(row)==1:
				i=1
				nil_count=row[0].lower().count('nil')
				while nil_count>0:
					row.append('Nil')
					row[0]=row[0].replace('NIL','')
					row[0]=row[0].replace('Nil','')
					row[0]=row[0].replace('nil','')
					nil_count-=1
				
				trace_count=row[0].lower().count('trace')
				while trace_count>0:
					row.append('trace')
					row[0]=row[0].replace('trace','')
					row[0]=row[0].replace('traces','')
					row[0]=row[0].replace('Trace','')
					row[0]=row[0].replace('Traces','')
					trace_count-=1

				while(len(csv_data)>rownum+i and len(csv_data[rownum+i])==1):
					if any(value.lower() in "".join(csv_data[rownum+i]).lower() for value in norm_ntr):
						break
					x= re.search(r"^(\d)+(.\d)*[ A-Za-z]*",csv_data[rownum+i][0])
					if x:
						csv_data[rownum].append(x.group(0))
						csv_data[rownum+i][0]=""
						# csv_data.pop([rownum+i][0])
					elif 'NIL'.lower() in csv_data[rownum+i][0].lower():
						csv_data[rownum].append('NIL')
						csv_data[rownum+i][0]=""
					elif 'Traces'.lower() in csv_data[rownum+i][0].lower():
						csv_data[rownum].append('traces')
						csv_data[rownum+i][0]=""
					elif any(value.lower() in csv_data[rownum+i][0].lower() for value in norm_unit_list):
						csv_data[rownum][-1]+=" "+csv_data[rownum+i][0].lower() 
						csv_data[rownum+i][0]=""
						# csv_data.pop([rownum+i][0])
					i+=1
		# print "After merge"
		# for rownum,row in enumerate(csv_data):
		# 	print row
		# 	# if "nutrition" or "supplement" in ''.join(row).lower():
		# 	# 	start_flag=1
		# 	# 	print file
		# 	# print row
		# 	# 	break
		print "Later"
		for rownum,row in enumerate(csv_data):
			print row
			except_row=[]
			for inIndex,inputString in enumerate(row):
				row[inIndex]=''.join([i if ord(i) < 128 else '' for i in inputString])
				row[inIndex]=re.sub('[^A-Za-z0-9.% ]+', '', row[inIndex])
			fl=0
			if all(value in ''.join(row).lower() for value in norm_serv_size) and not any(value in ''.join(row).lower() for value in not_norm_serv_size):
				
				serv_size.append(''.join(row))
				# except_row.append(inIndex)
				fl=1
			elif any(value in ''.join(row).lower() for value in norm_serv_cont):
				serv_container.append(''.join(row))
				# except_row.append(inIndex)
				fl=1
			if fl==1:
				continue
			# if rownum in except_row:
			# 	continue

		# print serv_size
		# print serv_container
			if 'Omg' in row[0]:
					row.insert(1,'0mg')
					row[0]=row[0].replace('Omg','')
			if 'omg' in row[0]:
				row.insert(1,'0mg')
				row[0]=row[0].replace('omg','')
			if 'Og' in row[0]:
				row.insert(1,'0g')
				row[0]=row[0].replace('Og','')
			if 'og' in row[0]:
				row.insert(1,'0g')
				row[0]=row[0].replace('og','')
			if len(row)>1:
				new_row=[]
				if 'vitamin' in row[0].lower() and any(value.lower() in row[1].lower() for value in ['b6','b1','b2','b3','b6','b12']):
					try:
						bval=re.search(r'^[bB](\d)+',row[1]).group(0)
						row[0]+=" "+bval
						row[1]=row[1].replace(bval,"")
						flag=re.search(r"\d+",row[1])
						if not flag:
							row.pop(1)
					except:
						print ""
				


				for index,key in enumerate(row):
					row[index]=row[index].replace("----","")
					row[index]=row[index].decode('utf-8','ignore')
					if "cal" not in row[0].lower() and "energy" not in row[0].lower():
						if not any(value.lower() in row[0].lower() for value in norm_unit_list):
							row[index] = re.sub('([\d\.]*\s*[\(]*)(9)$',r'\1'+'g', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(0)$',r'\1'+'g', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(q)$',r'\1'+'g', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(96)$',r'\1'+'%', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(90)$',r'\1'+'%', row[index].rstrip())
					row[index]=re.sub('(\d)*%$',"",row[index]) 
				flag=0
				unit=[]
				if row[0].lower() in norm_ntr:
					for u in norm_unit_list:
						if " "+u in key[0].lower():
							flag=1
							for inp,j in enumerate(row[1:]):
								row[inp]=row[inp].replace(u,"")
							break
				if flag==0:
					for u in norm_unit_list:
						if u.lower() in "".join(row[1:]).lower():
							row[0]=row[0]+" "+u.lower()
							for inp,j in enumerate(row[1:]):
								row[inp+1]=j.lower().replace(u.lower(),'')
							break

				

				for key in row:
					new_row.append(key)					
				nut_info.append(new_row)
			elif any(value.lower() in " ".join(row).lower() for value in norm_ntr):
				nut_info.append(row)
			



		print file
		# print serv_size
		# print serv_container
		print nut_info
		max=3
		worksheet_out.write(cur_row,0,file.strip(".csv"))
		worksheet_out.write(cur_row,2,'\n'.join(serv_size))
		worksheet_out.write(cur_row,3,'\n'.join(serv_container))
		for upp_ind,x in enumerate(nut_info):
			for ind,i in enumerate(x):
				# print i
				if file.strip('.csv').split('_')[-1]=='1' and ind>0:
					worksheet_out.write(cur_row+upp_ind,6,i)
				elif file.strip('.csv').split('_')[-1]=='2' and ind>0:
					worksheet_out.write(cur_row+upp_ind,5,i)
				else:
					worksheet_out.write(cur_row+upp_ind,4+ind,i)
				max=3+ind
		worksheet_out.insert_image(cur_row+1,max+15, "./cropped_23_2/"+file.rstrip(".csv")+".jpg",
			{'x_scale': 0.1, 'y_scale': 0.1})
		cur_row=cur_row+len(nut_info)+5
		worksheet_out.write(cur_row,0,"############")
		worksheet_out.write(cur_row,1,"############")
		cur_row=cur_row+1
	# except Exception as e:
	# 	print e
	# 	print "Skipped: "+file
workbook.close()	
