Labelweave was used for cropping nutrition tables and removing excess noise.
This improved QA verification efficiency.

xml_parse.py
For cropping images based on annotations from labelweave.
Labelweave has following name convention:- urlh_sno_(1 or 2 or 3):- 1- Amount per serving, 2- Per 100g, 3- both are present

correction_labelweave.py
Similar as standard one. 
Just uses image convention name to place values in their correct place (Amount per serving or per 100g)
