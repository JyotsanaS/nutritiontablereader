import csv
import json
import xlsxwriter
import os
import csv

import xlrd

sheet_num = 0
input_total = 0
output_total = 0

# path to the file you want to extract data from
src = './variant_raw/var8.xlsx'
sno=41
book = xlrd.open_workbook(src)
work_sheet = book.sheet_by_index(sheet_num)


workbook = xlsxwriter.Workbook("./variant_output_false/"+src.split('/')[2])
worksheet_out = workbook.add_worksheet('Sample_Data') 
cur_row=0

num_rows=work_sheet.nrows 
prev_col_0=""
prev_col_1=""
prev_col_2=""
prev_col_3=""

current_row = 1
urlhList=[]
while current_row < num_rows:
	if work_sheet.cell_value(current_row, 0)!='' and work_sheet.cell_value(current_row, 0)!='############':
		urlhList.append(work_sheet.cell_value(current_row, 0).split('_')[0])
	current_row+=1
print len(urlhList)
print urlhList

col_1=['']*len(urlhList)
col_2=['']*len(urlhList)
with open("../data/wholeAmazon/amazon_nutri_product_0410.jsonimage_desc.json","r") as fin:
	for lines in fin:		
		line=json.loads(lines)
		urlh=line['urlh']
		if urlh in urlhList:
			index=urlhList.index(urlh)
			meta=line['meta']
			title=line['title']
			col_1[index]=title
			col_2[index]=meta
print col_1
print col_2

current_row = 1
flagList=[]
starttable=[1]
while current_row < num_rows:
	if work_sheet.cell_value(current_row,1)=='############':
		starttable.append(current_row)
	current_row+=1
print len(flagList)
print flagList
print starttable

current_row = 1
cur_table=0
while current_row < num_rows:
	if current_row==starttable[cur_table]:
		flag=0
		if current_row==starttable[-1]:
			break
		while current_row<starttable[cur_table+1]:
			if work_sheet.cell_value(current_row,1)!='' and work_sheet.cell_value(current_row,1)!='############':
				flag=1
				flagList.append(work_sheet.cell_value(current_row,1))
			current_row+=1
		if flag==0:
			flagList.append(0.0)
	cur_table+=1
print flagList
print urlhList
print "Length"
print len(flagList)
print len(urlhList)




current_row=1
cur_table=0
col_3=[]
col_4=[]
while current_row<num_rows:
	if current_row==starttable[cur_table]:
		flag_3=0
		flag_4=0
		if current_row==starttable[-1]:
			break
		while current_row<starttable[cur_table+1]:
			if work_sheet.cell_value(current_row,2)!='' and flag_3==0:
				flag_3=1
				col_3.append(work_sheet.cell_value(current_row,2))
			if work_sheet.cell_value(current_row,3)!='' and flag_4==0:
				flag_4=1
				col_4.append(work_sheet.cell_value(current_row,3))
			current_row+=1
		if flag_3==0:
			col_3.append("")
		if flag_4==0:
			col_4.append("")
		cur_table+=1
print col_3
print col_4


current_row=1
cur_table=0
write_cur_row=0

while current_row<num_rows:
	if current_row==starttable[cur_table]:
		if current_row==starttable[-1]:
			break
		if flagList[cur_table]==1.0:
			cur_table+=1
			current_row=starttable[cur_table]
			continue
		else:
			sno+=1
		
		while current_row<starttable[cur_table+1]:
			if work_sheet.cell_value(current_row,4)!='' and (work_sheet.cell_value(current_row,5)!='' or work_sheet.cell_value(current_row,6)!=''):
				worksheet_out.write(write_cur_row,0,sno)
				worksheet_out.write(write_cur_row,1,urlhList[cur_table])
				worksheet_out.write(write_cur_row,2,col_1[cur_table])
				worksheet_out.write(write_cur_row,3,col_2[cur_table])
				worksheet_out.write(write_cur_row,4,col_3[cur_table])
				worksheet_out.write(write_cur_row,5,col_4[cur_table])
				worksheet_out.write(write_cur_row,6,work_sheet.cell_value(current_row,4))
				worksheet_out.write(write_cur_row,7,work_sheet.cell_value(current_row,5))
				worksheet_out.write(write_cur_row,8,work_sheet.cell_value(current_row,6))
				write_cur_row+=1
			current_row+=1
		cur_table+=1

workbook.close()