import csv
import os
import json
import re
import xlsxwriter

src_folder="merge.json"
# src_folder="./exp/"
# dest_folder="./final_csv_output/"


workbook = xlsxwriter.Workbook('merge.xlsx')
worksheet_out = workbook.add_worksheet('Final_data') 
cur_row=0

j=1
with open("merged.json") as fout:
	for i in fout:

		data=json.loads(i)
		title=data['title']
		subcategory=data['subcategory']
		brand=data['brand_tagged']
		prod_tagged=data['product_type_tagged']
		meta=data['meta']
		productType=data['productType']
		def_seller=data['default_seller']
		worksheet_out.write(cur_row,0,str(j))
		worksheet_out.write(cur_row,1,title)
		worksheet_out.write(cur_row,2,meta)
		worksheet_out.write(cur_row,3,subcategory)
		worksheet_out.write(cur_row,4,productType)
		worksheet_out.write(cur_row,5,brand)
		worksheet_out.write(cur_row,6,prod_tagged)
		worksheet_out.write(cur_row,7,def_seller)
		worksheet_out.write(cur_row,8,"Serving Size: "+str(data['Nutrition_Info']['Serving_size'])+"\n"+"Serving per container: "+str(data['Nutrition_Info']['Serving_per_container']))
		nutri_info=data['Nutrition_Info']['ntr']
		print nutri_info
		nutri_string=""
		for x in nutri_info:
			for k,v in x.items():
				if v['amt_per_serving'] is None or v['amt_per_serving']=='':
					v['amt_per_serving']="None"
				if v['per_100g'] is None or v['per_100g']=='':
					v['per_100g']="None"
				nutri_string=nutri_string+k+"-"+"amt per serving: "+v['amt_per_serving']+", per 100g: "+v['per_100g']+"\n"
		worksheet_out.write(cur_row,9,nutri_string)
		cur_row=cur_row+1
		j+=1


workbook.close()

	
