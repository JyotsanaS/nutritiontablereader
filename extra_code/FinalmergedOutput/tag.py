import os
import json
import xlrd
import xlsxwriter
import re

current_row = 3
sheet_num = 0
src = 'Batch5_final.xlsx'
# src='exp.xlsx'
book = xlrd.open_workbook(src)
work_sheet = book.sheet_by_index(sheet_num)

num_rows = work_sheet.nrows
sno=[]
done_sno=[]
while current_row < num_rows:
	print "curr_row:"+str(current_row)
	col_0=work_sheet.cell_value(current_row, 0)
	col_1=work_sheet.cell_value(current_row, 1)
	col_2=work_sheet.cell_value(current_row, 2)
	col_3=work_sheet.cell_value(current_row, 3)
	col_4=work_sheet.cell_value(current_row, 4)
	if col_0 in done_sno:
		current_row+=1
		continue
	else:
		done_sno.append(col_0)
	# print col_0,col_1,col_2,col_3,col_4
	data={}
	# print sno
	if col_0 not in sno:
		flag=0
		# with open("../../../data/old/amazon_nutrition_product.json","r") as fin:
		with open("../../../data/wholeAmazon/amazon_nutri_product_0410.json","r") as fin:
					for lines in fin:		
						if flag==1:
							break
						line=json.loads(lines)
						try:
							title=line['title']
							title=re.sub(' +',' ',title)
							title=title.strip(' ')
							col_2=re.sub(' +',' ',col_2)
							col_2=col_2.strip(' ')
						except:
							continue
						if col_2 == title:
							data['title']=title
							data['meta']=col_1
							data['Nutrition_Info']={}
							data['Nutrition_Info']['Serving_size']=col_3
							data['Nutrition_Info']['Serving_per_container']=col_4
							try:
								data['urlh']=line['urlh']
							except:
								data['urlh']=None
							try:
								data['productType']=line['product_type']
							except:
								data['productType']=None
							try:
								data['subcategory']=line['subcategory']
							except:
								data['subcategory']=None
							try:
								data['url']=line['url']
							except:
								data['url']=None
							try:
								data['default_seller']=line['default_seller']
							except:
								data['default_seller']=None
							sno.append(col_0)

							# Logic for Nutrition info
							cur_sno=sno[-1]
							data['Nutrition_Info']['ntr']=[]
							while cur_sno==sno[-1]:
								print current_row
								col_5=work_sheet.cell_value(current_row, 5)
								col_6=work_sheet.cell_value(current_row, 6)
								try:
									col_7=work_sheet.cell_value(current_row, 7)
								except:
									col_7=''
								data['Nutrition_Info']['ntr'].append({col_5:{'amt_per_serving':col_6,'per_100g':col_7}})
								current_row+=1
								if current_row<num_rows:
									cur_sno=work_sheet.cell_value(current_row, 0)
								else:
									break
							flag=1
							current_row-=1
							print data
							with open('data_5.json', 'a') as outfile:
								json.dump(data, outfile)
								outfile.write("\n")

	current_row+=1

