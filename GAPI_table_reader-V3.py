
# coding: utf-8

import json
import math
import re
import csv
import os
import config as parameter

def layoutAnalysis(inputFile=None,resultFile=None):
	out = {}
	with open(inputFile) as json_data:
		out = json.load(json_data)
	extract_data = out['responses'][0]['textAnnotations']
	words = []
	y_locations = []
	x_locations = []
	x_locations_end = []
	for iter in extract_data[1:]:
		words.append(iter['description'])
		try:
			try:
				x_locations.append(iter['boundingPoly']['vertices'][0]['x'])
			except:
				x_locations.append(iter['boundingPoly']['vertices'][1]['x'])
			try:
				x_locations_end.append(iter['boundingPoly']['vertices'][2]['x'])
			except:
				x_locations_end.append(iter['boundingPoly']['vertices'][3]['x'])
			try:
				y_locations.append(iter ['boundingPoly']['vertices'][0]['y'])
			except:
				y_locations.append(iter ['boundingPoly']['vertices'][1]['y'])
		except:
			print "coord not found"

	#sorting based on x locations
	#x_locations, y_locations, words = zip(*sorted(zip(x_locations, y_locations, words)))    
	#sorting based on y locations
	y_locations, x_locations, x_locations_end, words = zip(*sorted(zip(y_locations, x_locations, x_locations_end, words)))    

	#print y_locations
	#print words
	lineMap = []

	i=0
	# creating lines based on y
	while (i < len(y_locations)):
		j = i+1
		line = []
		line.append(i)
		while (j< len(y_locations)):
	#        i_location = y_locations[i]
			if abs(y_locations[i]-y_locations[j]) < 8:
					line.append(j)
					j+=1
					i+=1
			else:
				break
		i+=1
		lineMap.append(line)

	k = 0
	lineMap_words = []
	lineMap_x_locs = []
	lineMap_x_locs_end = []

	for itr in lineMap:
		x_list = []
		for i in itr:
			x_list.append(x_locations[i])
		x_list, itr = zip(*sorted(zip(x_list, itr)))    
		lineMap[k] =itr
		#update word list
		lineWord = []
		lineXLocs = []
		lineXLocsEnd = []
		for i in itr:
			lineWord.append(words[i])
			lineXLocs.append(x_locations[i])
			lineXLocsEnd.append(x_locations_end[i])
			
		lineMap_words.append(lineWord)
		lineMap_x_locs.append(lineXLocs)
		lineMap_x_locs_end.append(lineXLocsEnd)
		
		k+=1
		
	#print lineMap_words[7]
	#print lineMap_x_locs[7]
	#print lineMap_x_locs_end[7] 

	#print lineMap_words
	#print y_locations
	#print lineMap_x_locs_end

	minval = 20
	print lineMap_words
	combine_words_map = []

	unit_list = ['g','gram','mg','kj','kcal','%','KJ','kJ','Kcal','Packet', 'Stick', 'Bag', 'Can', 'Cup', 'Bar', 'Package', 'Bottle', 'Pouch', 'oz(Ounce)', 'Pieces', 'tbsp', 'Box']

	parent_child_map = {"caloriesfromfat":["calories"],"saturatedfat":["totalfat","fat"],"saturates":["totalfat","fat"],"transfat":["totalfat","fat"],"trans":["totalfat","fat"],"polyunsaturatedfat":["totalfat","fat"],"polyunsaturatedfat":["totalfat","fat"],"polyunsatfat":["totalfat","fat"],"polyunsaturates":["totalfat","fat"],"monounsaturatedfat":["totalfat","fat"],"monounsatfat":["totalfat","fat"],"monounsaturates":["totalfat","fat"],"dietaryfiber":["totalcarbohydrate","totalcarb"],"sugars":["totalcarbohydrate","totalcarb"],"sugaralcohol":["totalcarbohydrate","totalcarb"],"solublefiber":["totalcarbohydrate","totalcarb"],"othercarbohydrate":["totalcarbohydrate","totalcarb"],"totalsugars":["totalcarbohydrate","totalcarb"],"insolublefiber":["totalcarbohydrate","totalcarb"]}

	#combining units with numbers
	for i in  range(0,len(lineMap_words)):
		line_words = []
		words = ""
		j = 0 
		while j < len(lineMap_words[i]):
			l = re.findall('\d+', lineMap_words[i][j])
			if len(l) >0 and len(lineMap_words[i]) > j+1:
				if lineMap_words[i][j+1] in unit_list:
					line_words.append(lineMap_words[i][j]+lineMap_words[i][j+1])
					j+=1
				else:
					line_words.append(lineMap_words[i][j])
			else:
				line_words.append(lineMap_words[i][j])
			j+=1
		combine_words_map.append(line_words)
	print "-----------------unit combine---------------------------------"
	print combine_words_map
			
	combine_words_final = []

	special_cases = ['B6','B12','B3','B5','D3']
	special_cases_vitamin = ['vitaminb6','vitaminb12','vitaminb3','vitaminb5','vitamind3']

	for i in  range(0,len(combine_words_map)):
		line_words = []
		words = ""
		for j in combine_words_map[i]:
			l = re.findall('\d+', j)
			if len(l) >0 and j not in special_cases:
				if  words != "":
					line_words.append(words)
				line_words.append(j)
				words = ""

			else:
				if words != "":
					words += " "
				words+=j
		if words!="":
			line_words.append(words)

		combine_words_final.append(line_words)

	print "---------------------digit seperated-----------------------------"
	print combine_words_final 


	def write_line(line,writer):
		norm_j =''.join(e for e in line[0].lower() if e.isalnum())

		if re.findall('\d+', line[0]) and norm_j not in special_cases_vitamin:
			writer.writerow(line)
			print "return direct",norm_j
			return
		key_val = []
		#key_val.append(line[0])
		digit_flag = 0
		str_flag = 0

		for j in line:
			l = re.findall('\d+', j)
			norm_j =''.join(e for e in j if e.isalnum())

			if len(l)>0 and norm_j.lower() not in special_cases_vitamin:
				digit_flag =1
				key_val.append(j)
			else:
				if digit_flag:
					writer.writerow(key_val)
					print key_val
					key_val = [j]
					digit_flag = 0
					str_flag = 0
				else:
					key_val.append(j)
					str_flag =1
		#if digit_flag or str_flag:
		writer.writerow(key_val)


				
	#--------------------------------- json/CSV writer-------------------
	with open(resultFile, 'w') as csvfile:
		writer = csv.writer(csvfile, delimiter=',')
		parent = ""
		iter_on_line=0
		for line in combine_words_final:
			line = [item.encode('utf-8') for item in line]
			
			key = ''.join(e for e in line[0].lower() if e.isalnum())
			print key
			if key in parent_child_map:
				if parent.lower() not in parent_child_map[key]:
					parent_key=''.join(e for e in combine_words_final[iter_on_line-1][0].lower() if e.isalnum())

					if  parent_key in parent_child_map[key]:
						#parent = combine_words_final[iter_on_line-1][0]
						parent = parent_key
						line[0] = "----"+line[0]
						write_line(line,writer)
					else:
						write_line(line,writer)
						parent = ""
				else:
					line[0] = "----"+line[0]
					write_line(line,writer)
					
			else:
				write_line(line,writer)
				parent = ""

			iter_on_line+=1

	print "-----------------------------#"
	for i in  range(0,len(lineMap_words)):
		initial_space = lineMap_x_locs[i][0]-minval
		printer = ' ' * (initial_space/15)
		printer = printer + lineMap_words[i][0]
		lenToPixel = len(lineMap_words[i][0])/float(lineMap_x_locs_end[i][0]+1 - lineMap_x_locs[i][0])
		#print "R",lenToPixel
		for j in range (0,len(lineMap_words[i])-1):
			space = lineMap_x_locs[i][j+1] - lineMap_x_locs_end[i][j]
			if(space < 15):
				space = 15
			printer += ' ' * int(space/15)
			
			printer += lineMap_words[i][j+1]
		#print "----------------------------------------------------------------"
		print ''
		print printer

			
if __name__ == "__main__":
	source_folder=parameter.ocrSrc_Extractor
	output_folder=parameter.csvOutput_Extractor
	if not os.path.exists(output_folder):
		os.makedirs(output_folder)
	for root, dirs, files in os.walk(source_folder):
	 	for item in files:
	 		print item
	 		inputPath=source_folder+item
	 		outputPath=output_folder+item.strip('.txt')+".csv"
	 		layoutAnalysis(inputPath,outputPath)