Rename_Images (Not Required)

File:-rename.py
Usage:-python rename.py 

It renames all images as urlh+"_"+str(index)+".jpg" where index is serial number of image in json file.


------------------------------------------------------------------------------------------------------------------------------------------------
NTRC_multithreading

File:- detect_parallel.py
Usage:-
python detect_parallel.py imageDirectory NumOfParallelProcess threshold
imageDirectory-Source Folder with all images
NumOfParallelProcess- Number of Process to be run in parallel
threshold- Minimum number of nutrition words to declare the image nutrition table.
By default these parameter values are taken from config.py

File:- checkNTR.py 
Supports detect_parallel.py
It runs tesseract to detect number of nutrition words and based on threshold value declare the image as Nutrition Table or not. If True, image is put in 'passed' folder else in 'failed' folder.
In future, we can extend headWordList to contain more nutrition words.

------------------------------------------------------------------------------------------------------------------------------------------------
Nutrition Table Reader

File:- visionApi.py
Usage:-
python visionApi.py
Uses these parameters from config file
passedImgDir- Candidate Images directory on which OCR needs to be runned, Passed output of NTR. You can remove incorrect images from passed folder.(Ingredient etc.)
ocrOutput- Output of google vision ocr on image is stored in this folder.

File:-GAPI_table_reader-V3.py
Usage:-
python GAPI_table_reader-V3.py
Uses text output from ocr and word coordinates to get layout information and key-value pairs in csv format.
Process:-
1. Give some threshold for y, and get words lying in same row.
2. Arranges words in ascending order based on x coordinates.
3. Extract key-value pairs (Code has comments)
Input:- Directory containing text files from OCR- ocrSrc_Extractor (config)
Outut:- CSV file- csvOutput_Extractor (config)

File:-correction.py
Usage:-
python correction.py
Extracts nutrition information, serving size and serving per container from csv file.
Corrects common ocr errors and perform basic cleaning operations.
Output is in form of excel sheet or json depending on their correspnding flags in config file.
Parameters:-
excelOutputDir- Directory for keeping QA verification sheets
imgFolder- Images for displaying in QA sheet
excelFlag- If true make sheets of 150 images each for QA Verification
jsonFlag- To get json dump





