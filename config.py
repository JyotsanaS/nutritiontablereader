#Rename Images with urlh+sno+".jpg"
#So that we can keep track of urlh
jsonFile="../DW/rainman/data/wholeAmazon/amazon_nutri_product_0410.jsonimage_desc.json"
imgpath="../DW/rainman/data/wholeAmazon/Amazon/"
rename_img_savepath="./rename/"

#For Running Nutrition Table Classifier on Product Page Images
#Ensure Image name is correct and protects mapping. Final Output will have same mapping
#Runs tesseract on all images
#NumOfProcess- number of process to run in parallel
#srcImgDir- Directory of Images to be classified. Directory should be only 1 level (Dir-->All images)
#thresholdNTRC- Minimum number of keywords/words required to declare Image has nutrition table
NumOfProcess=4
srcImgDir=rename_img_savepath
thresholdNTRC=4


#Running Google OCR on passed images
#BE CAREFUL TO RUN THIS MODULE ONLY ONCE 
#passedImgDir- Candidate Images directory on which OCR needs to be runned, Passed output of NTR
#ocrOutput- Directory which will contain text output of Google OCR on each candidate image
passedImgDir='./passed/'
ocrOutput='./ocrOutput/'

#Extracting Nutrient Information from OCR Output
#If you want excel output (150 images/sheet), set excelFlag as TRUE
#If you want json dump, set jsonFlag as TRUE, output file:-jsondump.json
ocrSrc_Extractor=ocrOutput
csvOutput_Extractor='./ocrCSV/'
excelOutputDir='./QAVerfication/'
imgFolder=passedImgDir
excelFlag=True
jsonFlag=True






