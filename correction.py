import csv
import os
import json
import re
import xlsxwriter
import config as parameter



src_folder=parameter.csvOutput_Extractor
excel_name='P'
verification_sheet=1
imgFolder=parameter.imgFolder

if parameter.excelFlag==True:
	if not os.path.exists(parameter.excelOutputDir):
		os.makedirs(parameter.excelOutputDir)
	workbook = xlsxwriter.Workbook('dummy.xlsx')
	worksheet_out = workbook.add_worksheet('QAVerification') 

if parameter.jsonFlag==True:
	nutrifout=open("jsondump.json","w")

count_file=0
for file in os.listdir(src_folder):
	if parameter.excelFlag==True:
		if count_file%150==0:
			workbook.close()
			workbook = xlsxwriter.Workbook(parameter.excelOutputDir+excel_name+str(verification_sheet)+'.xlsx')
			worksheet_out = workbook.add_worksheet('QAVerification') 
			cur_row=0
			verification_sheet+=1

	count_file+=1
	
	with open(src_folder+file,'r') as fin:
		
		print file
		reader = csv.reader(fin)
		flag=0
		csv_data=list(reader)
		norm_serv_size=["size","serving"]
		not_norm_serv_size=["container"]
		norm_serv_cont=["per container","per package","contains"]
		amt_per_serving=["amount per serving","amt"]
		norm_ntr=["calories","energy","saturated fat","vitamin","total fat","fat","trans fat","fat","trans","poly unsaturated fat","unsaturated fat","mono unsaturated fat","unsat fat","dietary fiber","sugar","fiber","solublefiber","other carbohydrate","total sugar","insolublefiber","calc",
		"calories","omega","calories from fat","protein","fat","mufa","pufa","total fat","carbohydrate","sugar","sugars,of which","energy","vitamin","saturated fat","carbohydrate","trans fat","cholesterol","sodium","total carbohydrate","dietary fiber","energy from fat","total fat","saturated fatty acids","monounsaturated fatty acids","polyunsaturated fatty acids","vitamin a","vitamin c","vitamin b1","vitamin b2","vitamin b3","vitamin b6","vitamin b12","folate","iron","soluble fiber","insoluble", "fiber","thiamine","riboflavin","niacin","zinc","copper","magnesium","folic acid","phosphorous","calcium","iodine","inositol","choline","potassium","chromium","molybdenum","taurine","polyunsaturated fat","monounsaturated fat","vitamin d","fat","trans","cholesterol","sodium","carbohydrate","dietary" ,"fiber","energy","total fat","saturated","mono", "unsaturated","polyunsaturated"]
		norm_unit_list = ['mg','mcg','g','(g)','gms','gram','ug','grams','kj','kcal','%','kJ','ml','packet', 'stick', 'Bag', 'Can', 'Cup', 'Bar', 'Package', 'Bottle', 'Pouch', 'oz(Ounce)', 'Pieces', 'tbsp', 'Box']
		ntr_morethan1=['saturated fat','total fat','trans fat','poly unsaturated fat','polyunsaturated fat','total ca']
		norm_header=["amount per serving","daily value","dv","100g","100 g"]
		amt_number=[]
		each_100_number=[]
		serv_size=[]
		serv_container=[]
		header=[]
		# Nutr_table
		start_flag=0
		header=0
		nut_info=[]
		print "Before"
		for rownum,row in enumerate(csv_data):
			print row

		for rownum,row in enumerate(csv_data):
			if any(value.lower() in row[0].lower() for value in norm_ntr) and len(row)==1:
				i=1
				# -------Replace nil in key and add it to value------------
				nil_count=row[0].lower().count('nil')
				while nil_count>0:
					row.append('Nil')
					row[0]=row[0].replace('NIL','')
					row[0]=row[0].replace('Nil','')
					row[0]=row[0].replace('nil','')
					nil_count-=1
				# -------Replace trace in key and add it to value------------
				trace_count=row[0].lower().count('trace')
				while trace_count>0:
					row.append('trace')
					row[0]=row[0].replace('trace','')
					row[0]=row[0].replace('traces','')
					row[0]=row[0].replace('Trace','')
					row[0]=row[0].replace('Traces','')
					trace_count-=1

				# -------If key is nutrition key, remove unit from value and concatenate to key in form of (..)----
				# -------Also merge values to previous line in case they don't have keys (key has more than 1 value)--
				while(len(csv_data)>rownum+i and len(csv_data[rownum+i])==1):
					if any(value.lower() in "".join(csv_data[rownum+i]).lower() for value in norm_ntr):
						break
					x= re.search(r"^(\d)+(.\d)*[ A-Za-z]*",csv_data[rownum+i][0])
					if x:
						csv_data[rownum].append(x.group(0))
						csv_data[rownum+i][0]=""
					elif 'NIL'.lower() in csv_data[rownum+i][0].lower():
						csv_data[rownum].append('NIL')
						csv_data[rownum+i][0]=""
					elif 'Traces'.lower() in csv_data[rownum+i][0].lower():
						csv_data[rownum].append('traces')
						csv_data[rownum+i][0]=""
					elif any(value.lower() in csv_data[rownum+i][0].lower() for value in norm_unit_list):
						csv_data[rownum][-1]+=" ("+csv_data[rownum+i][0].lower()+")"
						csv_data[rownum+i][0]=""
					i+=1
		
		print "Later"
		for rownum,row in enumerate(csv_data):
			print row
			except_row=[]

			for inIndex,inputString in enumerate(row):
				# --------Remove non-ascii alphabets--------------
				row[inIndex]=''.join([i if ord(i) < 128 else '' for i in inputString])
		
				# --------Remove %age values----------------------
				row[inIndex]=re.sub('[^A-Za-z0-9.% ]+', '', row[inIndex])
			
			# ---------To find serving size-----------------------
			# It should contain word from norm_serv_size and should not contain any word from non_norm_serv_size
			# If not found we keep this empty
			fl=0
			if all(value in ''.join(row).lower() for value in norm_serv_size) and not any(value in ''.join(row).lower() for value in not_norm_serv_size):
				serv_size.append(''.join(row))
				fl=1
			elif any(value in ''.join(row).lower() for value in norm_serv_cont):
				serv_container.append(''.join(row))
				fl=1
			if fl==1:
				continue


			# --------Correcting common error:- 0 written as o or O in values---------
			# After correction we append it to key or previous row
			if 'Omg' in row[0]:
					row.insert(1,'0mg')
					row[0]=row[0].replace('Omg','')
			if 'omg' in row[0]:
				row.insert(1,'0mg')
				row[0]=row[0].replace('omg','')
			if 'Og' in row[0]:
				row.insert(1,'0g')
				row[0]=row[0].replace('Og','')
			if 'og' in row[0]:
				row.insert(1,'0g')
				row[0]=row[0].replace('og','')


			if len(row)>1:
				new_row=[]
				
				# ------merging b6, b1 etc with vitamin------------
				if 'vitamin' in row[0].lower() and any(value.lower() in row[1].lower() for value in ['b6','b1','b2','b3','b6','b12']):
					try:
						bval=re.search(r'^[bB](\d)+',row[1]).group(0)
						row[0]+=" "+bval
						row[1]=row[1].replace(bval,"")
						flag=re.search(r"\d+",row[1])
						if not flag:
							row.pop(1)
					except:
						print ""
				

				for index,key in enumerate(row):
					print "Here",row

					# -----------Removing Parent-child relationship we put in GAPI code-------
					row[index]=row[index].replace("----","")
			
					# -----------Ignore utf-8 code, should no longer be needed-----------------
					row[index]=row[index].decode('utf-8','ignore')
				
					# -----------Common ocr error:- g read as 0, 9 or q, % read as 96 or 90-------------
					# So, except in keys with calorie or energy, we replace values ending part if found----
					if "cal" not in row[0].lower() and "energy" not in row[0].lower():
						if not any(value.lower() in row[0].lower() for value in norm_unit_list):
							row[index] = re.sub('([\d\.]*\s*[\(]*)(9)$',r'\1'+'g', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(0)$',r'\1'+'g', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(q)$',r'\1'+'g', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(96)$',r'\1'+'%', row[index].rstrip())
							row[index] = re.sub('([\d\.]*\s*[\(]*)(90)$',r'\1'+'%', row[index].rstrip())
				
					# -----------If % found in previous case remove the value---------------------------
					row[index]=re.sub('(\d)*%$',"",row[index]) 
				

				# -------If after above corrections any unit is found (2 cases with " " or without " ")----
				# Append it to key delete them
				flag=0
				unit=[]
				if row[0].lower() in norm_ntr:
					for u in norm_unit_list:
						if " "+u in key[0].lower():
							flag=1
							for inp,j in enumerate(row[1:]):
								row[inp]=row[inp].replace(u,"")
							break
				if flag==0:
					for u in norm_unit_list:
						if u.lower() in "".join(row[1:]).lower():
							row[0]=row[0]+" ("+u.lower()+")"
							for inp,j in enumerate(row[1:]):
								row[inp+1]=j.lower().replace(u.lower(),'')
							break
				for key in row:
					new_row.append(key)					
				nut_info.append(new_row)

			# If key in norm_ntr then only add it.
			elif any(value.lower() in " ".join(row).lower() for value in norm_ntr):
				nut_info.append(row)
			# Remove this if you don't want to keep keys not in ntr.
			else:
				nut_info.append(row)
			# -----------nut_info contains all the changes for nutrition info-----
			# -----------serv_size contains serving size info---------------------
			# -----------serv_container contains serving per container information

		print file
		print nut_info
# --------------------------------------------------------------------------------------------------------------


		if parameter.excelFlag==True:
			max=3
			worksheet_out.write(cur_row,0,file.strip(".csv"))
			worksheet_out.write(cur_row,2,'\n'.join(serv_size))
			worksheet_out.write(cur_row,3,'\n'.join(serv_container))
			for upp_ind,x in enumerate(nut_info):
				for ind,i in enumerate(x):
					worksheet_out.write(cur_row+upp_ind,4+ind,i)
					max=3+ind
			worksheet_out.insert_image(cur_row+1,max+15, imgFolder+file.rstrip(".csv")+".jpg",
				{'x_scale': 0.05, 'y_scale': 0.05})
			cur_row=cur_row+len(nut_info)+5
			worksheet_out.write(cur_row,0,"############")
			worksheet_out.write(cur_row,1,"############")
			cur_row=cur_row+1

		if parameter.jsonFlag==True:
			data={'file':file.strip(".csv"),
					'ServingSize':str('\n'.join(serv_size)),
					'ServingPerContainer':str('\n'.join(serv_container)),
					'NutritionInfo':nut_info}
			# print data
			nutrifout.write(json.dumps(data))
			nutrifout.write("\n")

if parameter.excelFlag==True:
	workbook.close()	

if parameter.jsonFlag==True:
	nutrifout.close()
os.remove("dummy.xlsx") 