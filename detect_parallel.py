import os
import sys
from multiprocessing import Process
from checkNTR import checkNTR
import config as parameter

if len(sys.argv)!=4:
	# print "Usage:"
	# print "python detect_parallel.py SrcImageDirectory NumOfParallelProcess Threshold"
	preProcessSplit=parameter.NumOfProcess
	directory=parameter.srcImgDir
	threshold=parameter.thresholdNTRC

else:
	directory = sys.argv[1]
	preProcessSplit = int(sys.argv[2])
	threshold=int(sys.argv[3])



list_of_images = []
coreList= []
for filename in os.listdir(directory):
	list_of_images.append(os.path.join(directory, filename))

docsWithSplit = [list_of_images[(i*len (list_of_images)/preProcessSplit):((i+1)*len (list_of_images)/preProcessSplit)] for i in xrange (preProcessSplit)]
for i in xrange(preProcessSplit):
	coreList.append(Process(target=checkNTR,args=([docsWithSplit[i],threshold])))


for i in xrange(preProcessSplit):
	print "Staring Process Number : ", i+1
        coreList[i].start()

for i in xrange(preProcessSplit):
	coreList[i].join()
        print "Ending Process Number : ", i+1


