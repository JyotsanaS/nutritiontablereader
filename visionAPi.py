from base64 import b64encode
from os import makedirs
import os
from os.path import join, basename
from sys import argv
import json
import requests
import config as parameter

ENDPOINT_URL = 'https://vision.googleapis.com/v1/images:annotate'

def make_image_data_list(image_filenames):
	"""
	image_filenames is a list of filename strings
	Returns a list of dicts formatted as the Vision API
		needs them to be
	"""
	img_requests = []
	for imgname in image_filenames:
		with open(imgname, 'rb') as f:
			ctxt = b64encode(f.read()).decode()
			img_requests.append({
					'image': {'content': ctxt},
					'features': [{
						'type': 'TEXT_DETECTION',
						'maxResults': 1
					}]
			})
	return img_requests

def make_image_data(image_filenames):
	"""Returns the image data lists as bytes"""
	imgdict = make_image_data_list(image_filenames)
	return json.dumps({"requests": imgdict }).encode()


def request_ocr(api_key, image_filenames):
	response = requests.post(ENDPOINT_URL,
							 data=make_image_data(image_filenames),
							 params={'key': api_key},
							 headers={'Content-Type': 'application/json'})
	return response


if __name__ == '__main__':
	apiKey = "AIzaSyBHHgGYNFx0qj5XAkLO26lcTLU0e6P7Hbk"
	source_folder=parameter.passedImgDir
	imglist=os.listdir(source_folder)
	if not os.path.exists(parameter.ocrOutput):
		os.makedirs(parameter.ocrOutput)
	for index,img in enumerate(imglist):
		imgPath=source_folder+img
		imageAdd=[imgPath]
		output = request_ocr(apiKey,imageAdd).json()
		outPath=parameter.ocrOutput+img.strip('.jpg')+".txt"
		with open(outPath,'w') as outfile:
			json.dump(output,outfile)