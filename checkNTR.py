# import the necessary packages
from PIL import Image
import pytesseract
import argparse
import cv2
import os
import sys
from difflib import SequenceMatcher
import re
from shutil import copyfile

def checkNTR(imgList,threshold=4):

	if not os.path.exists("./passed/"):
		os.makedirs("./passed/")
	if not os.path.exists("./failed/"):
		os.makedirs("./failed/")
	for img in imgList:
		image = cv2.imread(img)

		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		# write the grayscale image to disk as a temporary file so we can
		# apply OCR to it
		filename = img.split("/")[-1].split(".")[0]+"_test.png"
		print img

		cv2.imwrite(filename, gray)


		# load the image as a PIL/Pillow image, apply OCR, and then delete
		# the temporary file
		text = pytesseract.image_to_string(Image.open(filename),config='-psm 6')
		if not len(text):
	                copyfile(img,"failed/"+img.split("/")[-1])
			continue
	


		headWordList = [ 'Nutrition',
		'Facts',
		'Protien',
		'Fat',
		'Sodium',
		'Iron',
		'Size',
		'Serving',
		'Container',
		'Amount',
		'Calories',
		'Daily',
		'Saturated',
		'Trans',
		'Cholesterol',
		'Sodium',
		'Carbohydrate',
		'Dietary',
		'Fiber',
		'Sugars',
		'Protein',
		'VitaminA',
		'VitaminC',
		'Calcium',
		'Iron',
		'VitaminD',
		'VitaminE',
		'Thiamin',
		'Riboflavin',
		'Niacin',
		'VitaminB',
		'Folic',
		'Acid',
		'Vitamin',
		'Zinc',
		'Percent',
		'Values',
		'Polyunsaturated',
		'Monounsaturated',
		'Potassium',
		'Total',
		'Folate',
		'Biotin',
		'Panthothenate',
		'Phosphorus',
		'Iodine',
		'Magnesium',
		'Selenum',
		'Copper',
		'Manganese',
		'Chromium',
		'Molybdenum',
		'Chloride',
		'alcohol',
		'Soluble',
		'Insoluble',
		'Salt',
		'Energy'
		]

		openFile = text.splitlines()
		pass_flag = 0
		sentenceList = []
		match_words = []
		if re.search("nutri", openFile[0], re.IGNORECASE) or re.search("fact", openFile[0], re.IGNORECASE):
			pass_flag =1

		else:
			for line in openFile:

		    		line = re.sub("[^\w]", " ",  line).split()
		    		for l in line:
		        		l = ''.join(e for e in l if e.isalpha())
		        		if l !='':
		            			sentenceList.append(l)

			percentage = 0
			count = 0
			word = ""
			for y in sentenceList:
		    		percentage = 0

				for x in headWordList:
		        		m = SequenceMatcher(None, y.lower(), x.lower())
		        		if m.ratio() > 0.9:
		            			percentage = m.ratio()
		            			word = x

				if percentage > 0.9:
					match_words.append(word)
		os.remove(filename)

		if len(list(set(match_words))) >=threshold or pass_flag:
		        copyfile(img,"./passed/"+img.split("/")[-1])

		else:
			copyfile(img,"./failed/"+img.split("/")[-1])